import React from 'react'
import Repo from './Repo'


export default class User extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            projects: [],
            contributions: []
        }
    }

    componentDidMount() {
        fetch(`https://gitlab.com/api/v4/users/${this.props.profile.id}/projects?per_page=100`)
            .then(resp =>
                resp.json().then(proj => {
                    // console.log(proj)
                    this.setState({ projects: proj })
                })
            )

        fetch(`https://gitlab.com/api/v4/users/${this.props.profile.id}/events?per_page=100`)
            .then(resp =>
                resp.json().then(contrib => {
                    // console.log(contrib)
                    this.setState({ contributions: contrib })
                })
            )
    }

    render() {
        return (
            <div className="card col-sm-6 col-lg-4">
                <div className="card-body">
                    <img src={this.props.profile.avatar_url}
                        className="card-img-top"
                        alt={this.props.profile.username}
                    />
                    <h5 className="card-title" title={this.props.profile.username}>
                        <a href={this.props.profile.web_url}
                            title={this.props.profile.username}
                            target="_blank"
                            rel="noreferrer"
                        >
                            {this.props.profile.name}
                        </a>

                        <span className="badge badge-danger">
                            {this.state.contributions.length}
                        </span>
                    </h5>

                    <ul>
                        {this.state.projects.map((p, id) => (
                            <Repo key={id} project={p} />
                        ))}
                    </ul>
                </div>
            </div>
        )
    }
}

