import React from 'react'
import { OverlayTrigger, Popover } from 'react-bootstrap'


export default class Repo extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            commit: [],
            team: []
        }
    }

    componentDidMount() {
        fetch(`https://gitlab.com/api/v4/projects/${this.props.project.id}/repository/commits?per_page=100`)
            .then(resp =>
                resp.json().then(commit => {
                    // console.log(commit)
                    this.setState({ commit: commit })
                })
            )

        fetch(`https://gitlab.com/api/v4/projects/${this.props.project.id}/members?private_token=${process.env.REACT_APP_GITLAB_TOKEN}&per_page=100`)
            .then(resp =>
                resp.json().then(t =>
                    this.setState({ team: t })
                )
            )
    }

    date2str(date, withHours = false) {
        let d = new Date(date)
        let dt = ('' + d.getFullYear()) //.slice(-2)
            + '-'
            + ('0' + (d.getMonth() + 1)).slice(-2)
            + '-'
            + ('0' + d.getDate()).slice(-2)

        if (withHours) {
            dt += (
                ' - '
                + ('0' + d.getHours()).slice(-2)
                + ':'
                + ('0' + d.getMinutes()).slice(-2)
            )
        }

        return dt
    }

    render() {
        return (
            <li key={this.props.project.id}>

                {this.date2str(this.props.project.created_at)} -

                <a href={this.props.project.web_url}
                    title={this.props.project.description}
                    target="_blank"
                    rel="noreferrer"
                >
                    {this.props.project.name}
                </a>

                <OverlayTrigger
                    key="commit"
                    placement="auto"
                    overlay={
                        <Popover>
                            <Popover.Title as="h3">Commit</Popover.Title>
                            <Popover.Content>
                                {this.state.commit.map((c, id) => (
                                    <p key={id}>
                                        <b>{this.date2str(c.committed_date, true)} {c.author_name} :</b> {c.message}
                                    </p>
                                ))}
                            </Popover.Content>
                        </Popover>
                    }
                >
                    <span className="badge badge-success">
                        {this.state.commit.length}
                    </span>
                </OverlayTrigger>

                {this.state.team.length > 1 &&

                    <OverlayTrigger
                        key="team"
                        placement="auto"
                        overlay={
                            <Popover>
                                <Popover.Title as="h3">Team</Popover.Title>
                                <Popover.Content>
                                    {this.state.team.map((e, id) =>
                                        <p key={id}>{e.name}</p>
                                    )}
                                </Popover.Content>
                            </Popover>
                        }
                    >
                        <span className="badge badge-primary">
                            {this.state.team.length}
                        </span>
                    </OverlayTrigger>
                }
            </li>
        )
    }
}

