import React from 'react'
import User from './User'


export default class Group extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            group: { name: '', description: '' },
            users: []
        }
    }

    componentDidMount() {
        fetch(`https://gitlab.com/api/v4/groups/${process.env.REACT_APP_GITLAB_GROUP}?private_token=${process.env.REACT_APP_GITLAB_TOKEN}`)
            .then(g => {
                g.json().then(json => {
                    // console.log(json)
                    this.setState({ group: json })
                })
            })

        fetch(`https://gitlab.com/api/v4/groups/${process.env.REACT_APP_GITLAB_GROUP}/members?private_token=${process.env.REACT_APP_GITLAB_TOKEN}&per_page=100`)
            .then(g => {
                g.json().then(json => {
                    // console.log(json)
                    this.setState({
                        users: json.filter(j =>
                            !process.env.REACT_APP_EXCLUDED_USERLIST.includes(j.username)
                        )
                    })
                })
            })
    }

    render() {
        return (
            <div>
                <header>
                    <h1>{this.state.group.name}</h1>
                    <p>
                        {this.state.group.description}

                        <a href={this.state.group.web_url}
                            target="_blank"
                            rel="noreferrer"
                        >
                            {/*
                                https://icons.getbootstrap.com/icons/link-45deg/ 
                            */}
                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-link-45deg" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4.715 6.542L3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1.001 1.001 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4.018 4.018 0 0 1-.128-1.287z" />
                                <path d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 0 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 0 0-4.243-4.243L6.586 4.672z" />
                            </svg>
                        </a>
                    </p>

                    <span className="badge badge-danger">events</span>
                    <span className="badge badge-success">commits</span>
                    <span className="badge badge-primary">membres</span>
                </header>

                <main className="row">
                    {this.state.users.map((g, id) => (
                        <User key={id} profile={g} />
                    ))}
                </main>
            </div>
        )
    }
}
