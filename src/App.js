import './App.css'
import Group from './Group'

function App() {
  return (
    <div className="App container">
      <Group />
      <footer><a href="http://funworks.fr">Orel</a> &copy; 2020</footer>
    </div>
  )
}

export default App
